package forsale;

import java.util.*;

class ChineseInvestor implements Strategy {
    
    private int rounds = 0;
    private float[] roundWeights;
    private float[] maxPerRound;
    private float[] sellWeights;
    private float[] maxSell;
    private int[] buy_rank;
    private int numRounds;
    private int pn;
    private static final float bidMult = 1;
    private static final float sellThresh = 2f;
    boolean init = false;
    boolean sinit = false;
    
    @Override
    public int bid(PlayerRecord p, AuctionState a) {
        pn = a.getPlayers().size();
        
        if (!init) {
            init = true;
            switch (pn) {
                case 3: numRounds = 24 / pn; break;
                case 4: numRounds = 28 / pn; break;
                default: numRounds = 30 / pn; break;
            }
            int min = 31;
            int max = -1;
            roundWeights = new float[numRounds];
            maxPerRound = new float[numRounds];
            
            ArrayList<Card> allCards = new ArrayList();
            for (PlayerRecord pr : a.getPlayers()) {
                allCards.addAll(pr.getCards());
            }
            
            allCards.addAll(a.getCardsInAuction());
            allCards.addAll(a.getCardsInDeck());
            
            for (Card c : allCards){
                max = Math.max(c.getQuality(), max);
                min = Math.min(c.getQuality(), min);
            }
            
            for (int i = 0; i < numRounds; i++){
                int startCard = i * pn;
                int endCard = startCard + pn;
                
                int localMin = 31;
                int localMax = -1;
                
                for (int j = startCard; j<endCard; j++) {
                    localMax = Math.max(allCards.get(j).getQuality(), localMax);
                    localMin = Math.min(allCards.get(j).getQuality(), localMin);
                    System.out.print(allCards.get(j).getQuality() + " ");
                }
                
                maxPerRound[i] = localMax;
                
                roundWeights[i] = -localMax / (float)max + localMin / (float)min;
                
                System.out.print(roundWeights[i]);
                System.out.println();
            }
        }
        rounds = numRounds - a.getCardsInDeck().size() / a.getPlayers().size() - 1;
        
        if (maxPerRound[rounds] < 15)
            return 0;
        if (a.getCurrentBid() > bidMult * roundWeights[rounds]){
            return 0;
        }
        return a.getCurrentBid() + 1;
        
    }
    
    @Override
    public Card chooseCard(PlayerRecord p, SaleState s) {
        SaleState a = s;
        if (!sinit){
            sinit = true;
            rounds = 0;
            switch (pn) {
                case 3: numRounds = 24/pn; break;
                case 4: numRounds = 28/pn; break;
                default: numRounds = 30/pn; break;
            }
            int min = 0;
            int max = 15;
            sellWeights = new float[numRounds];
            maxSell = new float[numRounds];
            MPair[] buy_r = new MPair[numRounds];
            buy_rank = new int[numRounds];
            
            ArrayList<Integer> allCards = new ArrayList();
            for (PlayerRecord pr : a.getPlayers()) {
                allCards.add(pr.getCash());
            }
            allCards.addAll(a.getChequesAvailable());
            allCards.addAll(a.getChequesRemaining());
            
            for (int i = 0; i<numRounds; i++){
                int startCard = i * pn;
                int endCard = startCard + pn;
                
                int localMin = 31;
                int localMax = -1;
                
                
                for (int j = startCard; j<endCard; j++){
                    localMax = Math.max(allCards.get(j), localMax);
                    localMin = Math.min(allCards.get(j), localMin);
                    System.out.print(allCards.get(j)+" ");
                }
                
                maxSell[i] = localMax;
                
                sellWeights[i] = (localMax - localMin)/15f;
                System.out.print(" w: "+sellWeights[i] +" "+(localMax - localMin) );
                System.out.println();
                buy_r[i] = new MPair(sellWeights[i],i);
            }
            Arrays.sort(buy_r);
            for (int j = 0; j<numRounds; j++){
                buy_rank[(int)buy_r[j].i] = j;
            }
            System.out.println(Arrays.toString(buy_rank));
        }
        rounds = numRounds - a.getChequesRemaining().size() / a.getPlayers().size() - 1;
        Card sell = p.getCards().get(0);
        if (buy_rank[rounds] >= sellThresh){
            // sell our best
            System.out.println("SELLING");
            for (Card c : p.getCards()){
                if (c.getQuality() > sell.getQuality())
                    sell = c;
            }
        }else{
            // sell our worst
            for (Card c : p.getCards()){
                if (c.getQuality() < sell.getQuality())
                    sell = c;
            }
        }
        
        return sell;
    }
    
    private class MPair<Float, Integer> implements Comparable{
        public final float f;
        public final Integer i;
        
        public MPair(float f, Integer i){
            this.f = f;
            this.i = i;
        }
        
        @Override
        public int compareTo(Object p){
            return (f > ((MPair)p).f? 1 : -1);
        }
        
        @Override
        public int hashCode() {
            return i.hashCode(); }
        
        @Override
        public boolean equals(Object o) {
            if (!(o instanceof MPair)) return false;
            MPair pairo = (MPair)o;
            return this.f == (pairo.f) && this.i == (pairo.i);
        }
    }
}
